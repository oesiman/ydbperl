# YDB Perl Plugin

## Overview

YDBperl is a plugin that allows M application code to use Perl.


When installed in the `$ydb_dist/plugin` directory, YDBperl consists of the following files:

- `libydbperl.so` – a shared library with the C software shims

- `ydbperl.xc` – a call-out table to allow M code to call the functions in `libydbperl.so`

- `r/_perlapi.m` – M source code for higher level `^%perlapi` entryrefs that M application code can call.

- `r/_perlapitest.m` – M source code for `%perlapitest.m` routine to test plugin with `mumps -run test^%perlapitest.m`

- `r/_excel.m` – M source code for `%excel` routine to test the excel-export `mumps -run test^%excel`

- `o/_ydbperl.so` – a shared library with M mode object code for `^%perlapi` & `^%perlapitest` & `^%excel` entryrefs

- `o/utf8/_ydbperl.so` – if YottaDB is installed with UTF-8 support, a shared library with UTF-8 mode object code

## Installing YDB Perl Plugin

YottaDB must be installed and available before installing the Perl plugin. https://yottadb.com/product/get-started/ has instructions on installing YottaDB. Download and unpack the Perl plugin in a temporary directory, and make that the current directory. Then:

```shell
source $(pkg-config --variable=prefix yottadb)/ydb_env_set
mkdir build_M && cd build_M
cmake ..
make && sudo make install
```

If YottaDB is installed with UTF-8 support, use these additional commands to install the plugin compiled for UTF-8 mode:

```shell
cd ..
mkdir build_UTF8 && cd build_UTF8
cmake -DM_UTF8_MODE=1 ..
make && sudo make install
```

After installing the Perl plugin, it is always a good idea to clear environment variables and set them again when you want to use the plugin, as the environment variables needed for the Perl plugin go beyond those for YottaDB itself.

```shell
source $(pkg-config --variable=prefix yottadb)/ydb_env_unset
```


At any time after installing the Perl plugin, you can always test it.

```shell
source $(pkg-config --variable=prefix yottadb)/ydb_env_set
mumps -run test^%perlapitest
```
