%perlapitest ; ugly.
        ;
        quit

test    ; test the perlapi
        do testFunc,testMeth,testArray,testUndef
        quit
        ;
testArray ;
        W "testing array ... "
        new rc,perror
        set perror=""
        set rc=$$push^%perlapi("hallo","one")
        set rc=$$push^%perlapi("hallo","two")
        set rc=$$push^%perlapi("hallo","three")
        write:"one"'=$$shift^%perlapi("hallo") "NOT "
        write:"three"'=$$pop^%perlapi("hallo") "NOT "
        write:"two"'=$$pop^%perlapi("hallo") "NOT "
        write:""'=$$pop^%perlapi("hallo") "NOT "
        W "OK",!
        quit
        ;
testFunc ;
        W "testing func ... "
        new out,perror
        set perror=""
        set out=$$eval^%perlapi("sub xfunc { join ""|"", @_ }")
        if perror'="" write "error eval: "_perror,! quit
        set out=$$call^%perlapi("xfunc",1,2,3,4,5,6,7,8)
        if perror'="" write "error call: "_perror,! quit
        if out="1|2|3|4|5|6|7|8" W "OK",! quit
        write "NOT OK",!
        quit
        ;;
        ;;;;;;
testMeth ;
        W "testing meth ... "
        new code,out,perror,xout
        set perror=""
        set code=$$load("methcode"),out=$$eval^%perlapi(code)
        if perror'="" write "error loading meth-code: "_perror,! quit
        if out'="Foo|1|2|3|4+5+6" write "not ok",! quit
        set out=$$meth^%perlapi("Foo","new",1,2,3,4,5,6,7,8)
        if perror'="" write "error running meth-code: "_perror,! quit
        set xout=$$meth^%perlapi(out,"bar",1,2,3,4,5,6,7,8)
        if perror'="" write "error running meth2-code: "_perror,! quit
        set out=$$meth^%perlapi(out,"crash",1,2,3,4,5,6,7,8)
        if perror'["division" write "error running meth3-code: "_perror,! quit
        set perror=""
        set rc=$&ydbperl.eval(.error,.out,"my $x = 1")
        if perror'="" write "error running eval2-code: "_perror,! quit
        write "OK",!
        quit
        ;
testUndef ;
        W "testing isundef ... "
        new perror,rc,ref
        set perror=""
        set rc=$$isundef^%perlapi("notareference")
        write:rc'=-1 "NOT "
        set ref=$$eval^%perlapi("\undef")
        set rc=$$isundef^%perlapi(ref)
        write:rc'=1 "NOT "
        write "OK",!
        quit
        ;
load(label) ;;;;;;
        new lines,x,l
        set lines=""
        for x=0:1:9999 set l=$P($T(@label+x),";",2,999) quit:l=""  set lines=lines_l_$C(10)
        quit lines
        ;
methcode ;package Foo;
        ;sub new { bless [@_] }
        ;sub bar { my $s = shift; return join "|", @$s, join "+", @_ }
        ;sub crash  { my $s = shift; 1/0; }
        ;package main;
        ;my $x = new Foo (1,2,3);
        ;$x->bar (4,5,6);
        ;package main;
        ;
