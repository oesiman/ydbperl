%excel  ;
        ; Example code. you need to install Spreadsheet::WriteExcel;
        ; on debian: "apt install libspreadsheet-writeexcel-perl"
        ;            or "cpan Spreadsheet::WriteExcel"
        ; Try "yottadb -r test^%excel" - it writes test.xls.
        ;
        quit
        ;
DieOnErr ;
        if $get(perror)'="" write perror,! halt
        quit
init    ;
        new perror
        if $$eval^%perlapi("use Spreadsheet::WriteExcel")
        do DieOnErr
        quit
        ;
newBook(filename)
        new perror,book
        set book=$$meth^%perlapi("Spreadsheet::WriteExcel","new",filename)
        do DieOnErr
        quit book
        ;
close(book)
        new perror
        if $$meth^%perlapi(book,"close")        
        do DieOnErr
        quit
        ;
newSheet(book,name)
        new perror,sheet
        set sheet=$$meth^%perlapi(book,"add_worksheet",$get(name))
        do DieOnErr
        quit sheet
        ;
image(sheet,row,col,img)
        new perror,meth
        if $$meth^%perlapi(sheet,"insert_image",row,col,img)
        do DieOnErr
        quit
        ;
write(sheet,row,col,what,type)
        new perror,meth
        set meth="write"_$select($data(type):"_"_type,1:"")
        if $$meth^%perlapi(sheet,meth,row,col,what)
        do DieOnErr
        quit
        ;
test    ;
        new perror,book,sheet,filename,row,col
        set filename="test.xls"
        do init
        set book=$$newBook(filename)
        set sheet=$$newSheet(book,"Mumps rulez!")
        for row=1:1:100 for col=1:1:100 do write(sheet,row,col,"Hello World! ("_row_","_col_")")
        do write(sheet,0,0,"https://yottadb.com","url")
        do write(sheet,1,0,3.1415926535,"number")
        do write(sheet,2,0,2,"number")
        do write(sheet,3,0,"=PRODUCT(A2:A3)","formula")
        ;do image(sheet,4,0,"/tmp/perl-logo.png")
        do close(book)
        write "wrote "_filename_" - done.",!
        do free^%perlapi(sheet)
        do free^%perlapi(book)
        quit
        ;
