%perlapi ;
         ;
        quit
isref(ref) ; checks if "ref" is a magic reference
        quit:($ZLENGTH(ref)=16!($ZLENGTH(ref)=24))&($EXTRACT(ref,1,8)=$CHAR(1,2,3,4,4,3,2,1)) 1 ; 64 bit
        quit:($ZLENGTH(ref)=8!($ZLENGTH(ref)=12))&($EXTRACT(ref,1,4)=$CHAR(1,2,2,1)) 1 ; 32 bit
        quit 0
        ;
svdump(sv) ; for debugging, calls Perl_sv_dump(sv)
        if '$$isref(sv) set perror="not a reference" quit
        if $&ydbperl.svdump(sv)
        quit
        ;
free(sv) ;
        if '$$isref(sv) set perror="not a reference" quit
        if $&ydbperl.freesv(sv)
        quit
        ;
eval(string) ;
        new rc,out
        set rc=$&ydbperl.eval(.perror,.out,string)
        goto call2r
        ;
call(func,a0,a1,a2,a3,a4,a5,a6,a7)
        new rc,out
        if '$data(a0) set rc=$&ydbperl.call(.perror,.out,func) goto call2r
        if '$data(a1) set rc=$&ydbperl.call(.perror,.out,func,a0) goto call2r
        if '$data(a2) set rc=$&ydbperl.call(.perror,.out,func,a0,a1) goto call2r
        if '$data(a3) set rc=$&ydbperl.call(.perror,.out,func,a0,a1,a2) goto call2r
        if '$data(a4) set rc=$&ydbperl.call(.perror,.out,func,a0,a1,a2,a3) goto call2r
        if '$data(a5) set rc=$&ydbperl.call(.perror,.out,func,a0,a1,a2,a3,a4) goto call2r
        if '$data(a6) set rc=$&ydbperl.call(.perror,.out,func,a0,a1,a2,a3,a4,a5) goto call2r
        if '$data(a7) set rc=$&ydbperl.call(.perror,.out,func,a0,a1,a2,a3,a4,a5,a6) goto call2r
        set rc=$&ydbperl.call(.perror,.out,func,a0,a1,a2,a3,a4,a5,a6,a7) goto call2r
        ;
call2r  ;
        if rc=-1,$get(perror)="" set perror="wrong number of arguments"
        quit:rc ""
        quit out
        ;
meth(obj,meth,a0,a1,a2,a3,a4,a5,a6,a7)
        new rc,out
        if '$data(a0) set rc=$&ydbperl.meth(.perror,.out,obj,meth) goto call2r 
        if '$data(a1) set rc=$&ydbperl.meth(.perror,.out,obj,meth,a0) goto call2r 
        if '$data(a2) set rc=$&ydbperl.meth(.perror,.out,obj,meth,a0,a1) goto call2r 
        if '$data(a3) set rc=$&ydbperl.meth(.perror,.out,obj,meth,a0,a1,a2) goto call2r 
        if '$data(a4) set rc=$&ydbperl.meth(.perror,.out,obj,meth,a0,a1,a2,a3) goto call2r 
        if '$data(a5) set rc=$&ydbperl.meth(.perror,.out,obj,meth,a0,a1,a2,a3,a4) goto call2r 
        if '$data(a6) set rc=$&ydbperl.meth(.perror,.out,obj,meth,a0,a1,a2,a3,a4,a5) goto call2r 
        if '$data(a7) set rc=$&ydbperl.meth(.perror,.out,obj,meth,a0,a1,a2,a3,a4,a5,a6) goto call2r 
        set rc=$&ydbperl.meth(.perror,.out,obj,meth,a0,a1,a2,a3,a4,a5,a6,a7) goto call2r 
        ;
push(array,value)
    new rc
    set rc=$&ydbperl.push(.perror,array,value)
    quit rc
    ;
pop(array)
    new rc,out
    set rc=$&ydbperl.pop(.perror,.out,array)
    goto call2r
    ;
shift(array)
    new rc,out
    set rc=$&ydbperl.shift(.perror,.out,array)
    goto call2r
    ;
isundef(sv)
    new rc
    set rc=$&ydbperl.isundef(sv)
    if rc=-1,perror="" set perror="not a reference" quit -1
    quit rc
    ;
