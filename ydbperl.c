
#include <EXTERN.h>
#include <perl.h>
#include "libyottadb.h"

// #define SVDEBUG 1

static PerlInterpreter *my_perl = 0;

extern void boot_DynaLoader (pTHX_ CV* cv);

static void
my_xs_init (pTHX)
{
	static const char file[] = __FILE__;
	dXSUB_SYS;
	PERL_UNUSED_CONTEXT;
	newXS("DynaLoader::boot_DynaLoader", boot_DynaLoader, file);
}

static char *embed[] = { "", "-e", "", 0 }, **pembed = embed;
static int embed_argc = 3;
extern char **environ;

static inline void perl_init (void)
{
	static bool for_the_very_first_time = 1;
	if (my_perl)
		return;

	if (for_the_very_first_time)
		PERL_SYS_INIT3(&embed_argc, &pembed, &environ);
	for_the_very_first_time = 0;

	my_perl = perl_alloc ();
	perl_construct (my_perl);
	PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
	perl_parse(my_perl, my_xs_init, embed_argc, embed, (char **)NULL);
	perl_run (my_perl);
}

/*
 * currently unused...
 */
static inline void perl_exit (void)
{
	if (!my_perl)
		return;
	perl_destruct (my_perl);
	perl_free (my_perl);
	// PERL_SYS_TERM();
	my_perl = 0;
}


static inline int intMIN(int i, int j)
{
	if (i < j)
		return i;
	return j;
}


static inline int error_copy (ydb_string_t *err)
{
	STRLEN len;
	char *s;
	s = SvPV (ERRSV, len);
	err->length = intMIN (err->length-1, len);
	memmove (err->address, s, err->length);
	err->address[err->length] = 0;
	return !!len;
}


// assume pointer-size is long
#ifdef SVDEBUG
# define SVREFSIZE (sizeof(long) * 3)
#else
# define SVREFSIZE (sizeof(long) * 2)
#endif


#if LONGSIZE == 8
# define MySVMagic 0x0102030404030201UL
#else
# define MySVMagic 0x01020201
#endif


static inline int is_magic (ydb_string_t *ystr)
{
	if (ystr
	    && ystr->length == SVREFSIZE
	    && *(long *) ystr->address == MySVMagic) {
		return 1;
	}
	return 0;
}


static inline long get_magic (ydb_string_t *ystr)
{
	if (!is_magic (ystr))
		return 0;
#ifdef SVDEBUG
	{
		SV *sv  = (SV *) ((long *) ystr->address)[1];
		SV *ssv = (SV *) ((long *) ystr->address)[2];
		if (!SvROK (sv)) {
			fprintf (stderr, "get_magic: FATAL: sv is not a reference anymore; dumping sv\n");
			sv_dump (sv);
			fflush (stderr);
			abort ();
		}
		if (SvRV (sv) != ssv) {
			fprintf (stderr, "get_magic: FATAL: sv pointer mismatch, dumping sv and ssv\n");
			sv_dump (sv);
			sv_dump (ssv);
			fflush (stderr);
			abort ();
		}
	}
#endif
	return ((long *) ystr->address)[1];
}


static inline SV* copy_ystr_to_sv (ydb_string_t *yst)
{
	SV *sv;
	long ptr = get_magic (yst);

	if (!ptr) {
		sv = newSVpvn (yst->address, yst->length);
	} else {
		sv = (SV *) ptr;
	}
	return sv;
}


static inline int do_copy_svref_to_ystr (SV *sv, ydb_string_t *target)
{
	SV *ssv;

	if (target->length < SVREFSIZE)
		return -1;

	ssv = SvRV (sv);
#ifdef SVDEBUG
	{
		u32 refcnt;

		if ((refcnt = SvREFCNT(sv)) < 1) {
			fprintf (stderr, "do_copy_svref_to_ystr: FATAL: refcount(sv)=%u\n", refcnt);
			fflush (stderr);
			abort ();
		}
		if ((refcnt = SvREFCNT(ssv)) < 1) {
			fprintf (stderr, "do_copy_svref_to_ystr: FATAL: refcount(ssv)=%u\n", refcnt);
			fflush (stderr);
			abort ();
		}
	}
#endif

	SvREFCNT_inc (ssv);
	SvREFCNT_inc (sv);

	*((long *) target->address) = MySVMagic;
	((long *) target->address)[1] = (long) sv;
#ifdef SVDEBUG
	((long *) target->address)[2] = (long) ssv;
#endif
	target->length = SVREFSIZE;

	return 0;
}


static inline int copy_sv_to_ystr (SV *sv, ydb_string_t *target)
{
	STRLEN len;
	char *s;

	if (SvROK (sv)) {
		return do_copy_svref_to_ystr (sv, target);
	}
	s = SvPV (sv, len);
	target->length = intMIN (target->length-1, len);
	memmove (target->address, s, target->length);
	target->address[target->length] = 0;
	return len == target->length; // truncated?
}


int mp_eval (int items, ydb_string_t *err, ydb_string_t *out, ydb_string_t *string)
{
	SV *sv, *errsv;

	if (items != 3)
		return -1;
	
	perl_init ();
	
	dSP;
	ENTER_with_name ("mp_eval");
	SAVETMPS;
	PUSHMARK (SP);

	errsv = get_sv("@", GV_ADD);
    	SvPVCLEAR(errsv);
	PUTBACK;
	sv = copy_ystr_to_sv (string);
	eval_sv (sv, G_EVAL | G_SCALAR | G_NOARGS);
	SPAGAIN;
	sv = POPs;

	copy_sv_to_ystr (sv, out);

        PUTBACK;
        FREETMPS;
        LEAVE_with_name ("mp_eval");

	return error_copy (err);
}


int mp_svdump (int items, ydb_string_t *string)
{
	SV *sv;

	if (items != 1)
		return -1;
	perl_init ();

	if (!(sv = (SV *)get_magic (string)))
		return 1;

	sv_dump (sv);

	return 0;
}


int mp_freesv (int items, ydb_string_t *string)
{
	long ptr;
	SV *sv,*ssv;

	if (items != 1)
		return -1;
	perl_init ();
	
	if (!(ptr = get_magic (string)))
		return 1;

	sv = (SV *) ptr;

	if (SvROK (sv)) {
		ssv = SvRV (sv);
		SvREFCNT_dec (ssv);
	} else {
		// arghh, should not happen...
	}

	SvREFCNT_dec (sv);

	return 0;
}


int mp_call (int items, ydb_string_t *errsv, ydb_string_t *out, ydb_string_t *func,
	     ydb_string_t *a0, ydb_string_t *a1, ydb_string_t *a2, ydb_string_t *a3,
	     ydb_string_t *a4, ydb_string_t *a5, ydb_string_t *a6, ydb_string_t *a7)
{
	int callargc;
	int i;
	SV *sv;
	ydb_string_t *args[] = { a0, a1, a2, a3, a4, a5, a6, a7 };

	if (items < 3)
		return -1;
	perl_init ();

	callargc = items - 3;

	dSP;
	ENTER_with_name ("mp_call");
	SAVETMPS;

	PUSHMARK (SP);
	if (callargc)
		EXTEND (SP, callargc);

	for (i = 0; i < callargc; i++) {
			PUSHs (copy_ystr_to_sv (args[i]));
	}

	PUTBACK;

	call_sv (copy_ystr_to_sv (func), G_SCALAR | (callargc ? 0 : G_NOARGS) | G_EVAL); // returns 1

	SPAGAIN;

	sv = POPs;
	copy_sv_to_ystr (sv, out);

	PUTBACK;
	FREETMPS;
	LEAVE_with_name ("mp_call");
	return error_copy (errsv);
}


int mp_meth (int items, ydb_string_t *errsv, ydb_string_t *out, ydb_string_t *obj,
	     const char *meth,
	     ydb_string_t *a0, ydb_string_t *a1, ydb_string_t *a2, ydb_string_t *a3,
	     ydb_string_t *a4, ydb_string_t *a5, ydb_string_t *a6, ydb_string_t *a7)
{
	int methargc;
	int i;
	SV *sv;
	ydb_string_t *args[] = { obj, a0, a1, a2, a3, a4, a5, a6, a7 };

	if (items < 4)
		return -1;
	perl_init ();

	methargc = items - 3;

	dSP;
	ENTER_with_name ("mp_meth");
	SAVETMPS;

	PUSHMARK (SP);
	EXTEND (SP, methargc);

	for (i = 0; i < methargc; i++) {
			PUSHs (copy_ystr_to_sv (args[i]));
	}
	
	PUTBACK;

	call_method (meth, G_SCALAR | G_EVAL); // returns 1

	SPAGAIN;

	sv = POPs;

	copy_sv_to_ystr (sv, out);

	PUTBACK;
	FREETMPS;
	LEAVE_with_name ("mp_meth");
	return error_copy (errsv);
}


int mp_array_push (int items, ydb_string_t *errsv, const char *array, ydb_string_t *value)
{
	SV *sv;
	AV *av;

	if (items != 3)
		return -1;

	perl_init ();

	sv = copy_ystr_to_sv (value);
	av = get_av (array, GV_ADD);
	av_push (av, sv);

	return error_copy (errsv);
}


int mp_array_pop (int items, ydb_string_t *errsv, ydb_string_t *out, const char *array)
{
	SV *sv;
	AV *av;

	if (items != 3)
		return -1;

	perl_init ();

	av = get_av (array, GV_ADD);
	sv = av_pop (av);
	copy_sv_to_ystr (sv, out);

	return error_copy (errsv);
}


int mp_array_shift (int items, ydb_string_t *errsv, ydb_string_t *out, const char *array)
{
	SV *sv;
	AV *av;

	if (items != 3)
		return -1;

	perl_init ();

	av = get_av (array, GV_ADD);
	sv = av_shift (av);
	copy_sv_to_ystr (sv, out);

	return error_copy (errsv);
}


int mp_is_undef (int items, ydb_string_t *obj)
{
	SV *sv;

	if (items != 1)
		return -1;

	perl_init ();

	sv = (SV *) get_magic (obj);
	if (!sv) {
		return -1;
	} else {
		if (SvROK(sv) && SvRV(sv) == &PL_sv_undef) {
			return 1;
		}
	}
	return 0;
}
